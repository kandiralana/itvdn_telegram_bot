FROM python:3.12 as base

LABEL maintainer="Anastasiia Kandiral <kandiralana@gmail.com>"

ARG UID=1000
ARG GID=1000
ENV UID=${UID}
ENV GID=${GID}

RUN useradd -m -u $UID docker_user

USER docker_user

WORKDIR /home/docker_user/app

ENV PYTHONDONTWRITEBYTECODE=1 PYTHONUNBUFFERED=1

COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY . .

CMD ["python3", "-m", "bot.main"]


